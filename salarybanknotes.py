with open('input', 'r') as salary:

    fivethousands = 0
    thousands = 0
    fivehundreds = 0
    hundreds = 0
    fifties = 0

    for number in salary:
        fivethousands += int(number) // 5000
        thousands += (int(number) % 5000) // 1000
        fivehundreds += ((int(number) % 5000) % 1000) // 500
        hundreds += (((int(number) % 5000) % 1000) % 500) // 100
        fifties += ((((int(number) % 5000) % 1000) % 500) % 100) // 50

    print(f'{fivethousands} по 5000р,\n{thousands} по 1000р,\n{fivehundreds} по 500р,\n{hundreds} по 100р,\n{fifties} по 50р.')